package christmastree;

import java.util.ArrayList;
import java.util.List;

public class App 
{
    public static void main( final String[] args) {
        App app = new App();
        for(String row : app.draw(5)){
            System.out.println(row);
        }
    }

    private static final int FIRST_TREESPACE = 1;
    private static final int TWO_SIDES = 2;

    public List<String> draw(final int height) {

        final List<String> list = new ArrayList<>();
        int whitespaces = firstWhitespaces(height);
        for (int i = 0; i < height; i++) {
            list.add(grow(whitespaces, height, false));
            --whitespaces;
        }
        list.add(grow(firstWhitespaces(height), height, true));
        return list;
    }

    private int firstWhitespaces(final int treeHeightWithoutStel) {
        final int firstWhitespaces = treeHeightWithoutStel;
        return firstWhitespaces;
    }

    public String grow(final int whitespaces, final int height, final boolean isStem) {
        final StringBuilder builder = new StringBuilder();

        for (int i = 0; i < whitespaces; ++i) {
            builder.append(" ");
        }

        for (int i = 0; i < treespaces(whitespaces, height); ++i) {
            if (isStem) {
                builder.append("I");
            } else {
                builder.append("X");
            }
        }

        for (int i = 0; i < whitespaces; ++i) {
            builder.append(" ");
        }
        return builder.toString();
    }

    public int treespaces(final int whitespaces, final int height) {
        final int space = firstWhitespaces(height) * TWO_SIDES + FIRST_TREESPACE;
        final int treespaces = space - whitespaces * TWO_SIDES;
        return treespaces;
    }
}
