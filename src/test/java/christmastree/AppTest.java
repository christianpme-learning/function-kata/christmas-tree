package christmastree;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class AppTest 
{
    @Test
    public void drawTest(){
        App app = new App();
        List<String> actual = app.draw(5);
        
        List<String> expected = Arrays.asList(  "     X     ",
                                                "    XXX    ",
                                                "   XXXXX   ",
                                                "  XXXXXXX  ",
                                                " XXXXXXXXX ",
                                                "     I     ");

        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void growTest(){
        App app = new App();
        String actual = app.grow(5, 5, false);

        String expected = "     X     ";

        assertTrue(expected.equals(actual));
    }

    @Test
    public void growWhitespace1Test(){
        App app = new App();
        String actual = app.grow(1, 5, false);

        String expected = " XXXXXXXXX ";

        assertTrue(expected.equals(actual));
    }

    @Test
    public void treespacesTest(){
        App app = new App();
        int actual = app.treespaces(5, 5);

        int expected = 1;

        assertTrue(actual==expected);
    }

    @Test
    public void treespacesWhitespace3Test(){
        App app = new App();
        int actual = app.treespaces(3, 5);

        int expected = 5;

        assertTrue(actual==expected);
    }

    @Test
    public void treespacesWhitespace1Test(){
        App app = new App();
        int actual = app.treespaces(1, 5);

        int expected = 9;

        assertTrue(actual==expected);
    }

    @Test
    public void treespacesHeightTest(){
        App app = new App();
        int actual = app.treespaces(6, 6);

        int expected = 1;

        assertTrue(actual==expected);
    }

    @Test
    public void treespacesWhitespace1HeightTest(){
        App app = new App();
        int actual = app.treespaces(1, 6);

        int expected = 11;

        assertTrue(actual==expected);
    }
}
